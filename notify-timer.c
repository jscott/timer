#define _XOPEN_SOURCE 700
#include <errno.h>
#include <glib.h>
#include <libgen.h>
#include <libnotify/notify.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <time.h>
#include <wctype.h>

int main(int argc, char *argv[]) {
	if(setlocale(LC_ALL, "") == NULL) {
		fputs("Failed to set default locale\n", stderr);
		exit(EXIT_FAILURE);
	} else if(!notify_init(basename(argv[0]))) {
		fputs("notify_init() failed\n", stderr);
		exit(EXIT_FAILURE);
	} else if(atexit(notify_uninit)) {
		fputs("atexit() failed\n", stderr);
		notify_uninit();
		exit(EXIT_FAILURE);
	}

	int exitstat = EXIT_FAILURE;

	char *line = NULL;
	size_t len = 0;
	(void)getline(&line, &len, stdin);

	if(ferror(stdin)) {
		perror("Failed to read time");
		goto endline;
	}

	struct tm broke_down = {0};
	char *suffix = strptime(line, "%X", &broke_down);

	time_t waitsec;
	if(suffix == NULL) {
		fputs("Failed to read time\n", stderr);
		goto endline;
	} else {
		waitsec = mktime(&broke_down) - mktime(&(struct tm){0});
		if(waitsec == -1) {
			perror("Failed to get seconds");
			exit(EXIT_FAILURE);
		}
	}


	if(thrd_sleep(&(struct timespec){.tv_sec = waitsec}, NULL) < 0) {
		fputs("Failed to sleep\n", stderr);
		goto endline;
	}

	/* Truncate the delimiter. */
	wchar_t delim;
	int delim_mblen = mbtowc(&delim, suffix, strlen(suffix));
	if(delim_mblen == -1) {
		perror("Failed to convert to wide character");
		goto endline;
	} else if(iswspace(delim)) {
		suffix += delim_mblen;
	}

	NotifyNotification *foof = notify_notification_new("Time is up", suffix, NULL);
	notify_notification_set_timeout(foof, NOTIFY_EXPIRES_DEFAULT);
	notify_notification_set_urgency(foof, NOTIFY_URGENCY_NORMAL);
	notify_notification_set_hint(foof, "sound-name", g_variant_new_string("alarm-clock-elapsed"));

	GError *feck = NULL;
	if(!notify_notification_show(foof, &feck)) {
		fprintf(stderr, "Failed to show notification: %s\n", feck->message);
		g_error_free(feck);
	} else {
		exitstat = EXIT_SUCCESS;
	}

	endline:
	free(line);
	exit(exitstat);
}
